from django.shortcuts import render
from showbooks.models import Book, Magazine, Genre
# Create your views here.
from showbooks.forms import BookForm, MagazineForm
from django.shortcuts import redirect, get_object_or_404


def show_books(request):
    books = Book.objects.all()
    context = {
        'books_var' : books
    }
    #return render(request, template, context)
    return render(request, "books/list.html", context)

def create_book(request):
    context = {}
    form = BookForm(request.POST or None)
    if form.is_valid():
            form.save()
            return redirect("book_list")
    context['form'] = form
    return render(request, 'books/create.html', context)
    
def show_a_book(request, pk):
    book = Book.objects.get(pk=pk)
    context = {
        'book_var' : book
    }
    return render(request, "books/details.html", context)

def edit_book(request,pk):
    context = {}
    obj = get_object_or_404(Book, id = pk)
    form = BookForm(request.POST or None, instance = obj)
    if form.is_valid():
        form.save()
        return redirect("book_list")
    context['form'] = form
    return render(request, "books/edit.html", context)


def delete_book(request, pk):
    context ={}
    obj = get_object_or_404(Book, id = pk)
    if request.method =="POST":
        obj.delete()
        return redirect("book_list")
    return render(request, "books/delete.html", context)


def show_magazines(request):
    mags = Magazine.objects.all()
    context = {
        'magazines_var' : mags
    }
    #return render(request, template, context)
    return render(request, "magazines/magazines_list.html", context)

def create_magazine(request):
    context = {}
    form = MagazineForm(request.POST or None)
    if form.is_valid():
            form.save()
            return redirect("magazines_list")
    context['form'] = form
    return render(request, 'magazines/magazines_create.html', context)

def show_a_magazine(request, pk):
    mag = Magazine.objects.get(pk=pk)
    context = {
        'magazine_var' : mag
    }
    return render(request, "magazines/magazines_details.html", context)

def edit_magazine(request,pk):
    context = {}
    obj = get_object_or_404(Magazine, id = pk)
    form = MagazineForm(request.POST or None, instance = obj)
    if form.is_valid():
        form.save()
        return redirect("magazines_list")
    context['form'] = form
    return render(request, "magazines/magazines_edit.html", context)

def delete_magazine(request, pk):
    context ={}
    obj = get_object_or_404(Magazine, id = pk)
    if request.method =="POST":
        obj.delete()
        return redirect("magazines_list")
    return render(request, "magazines/magazines_delete.html", context)

def show_genres_details(request, pk):
    genres = Genre.objects.get(pk=pk)
    context = {
        'genre_var' : genres
    }
    return render(request, "magazines/magazines_genres.html", context)
