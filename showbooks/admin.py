from django.contrib import admin
from showbooks.models import Book, Magazine, Author, BookReview, Genre, Issue

admin.site.register(Book)
admin.site.register(Magazine)
admin.site.register(BookReview)
admin.site.register(Author)
admin.site.register(Genre)
admin.site.register(Issue)