from django import forms
from showbooks.models import Book, Magazine

class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = [
            "title",
            "author",
            "number_page",
            "isbn",
            "in_print",
            "cover",
            "publish_date",
        ]


class MagazineForm(forms.ModelForm):
    class Meta:
        model = Magazine
        exclude = []


# This also works if you want to include all of your fields!
# It's essentially, exclude all of these (which is none) and include the rest 
# so it's all of them

# class BookForm(forms.ModelForm):
    # class Meta:
    #     model = Book
    #     exclude = []

