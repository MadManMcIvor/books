from django.contrib import admin
from django.urls import path, include
from showbooks.views import show_books, create_book, show_a_book, edit_book, delete_book, show_magazines
from showbooks.views import show_magazines, create_magazine, show_a_magazine, edit_magazine, delete_magazine, show_genres_details

urlpatterns = [
    path('', show_books, name="book_list"),
    path('create/', create_book, name="book_new"),
    path('<int:pk>/', show_a_book, name="book_details"),
    path('<int:pk>/edit/', edit_book, name="book_edit"),
    path('<int:pk>/delete/', delete_book, name="book_delete"),
    path('magazines/', show_magazines, name="magazines_list"),
    path('magazines/create/', create_magazine, name="magazine_new"),
    path('magazines/<int:pk>/', show_a_magazine, name="magazine_details"),
    path('magazines/<int:pk>/edit/', edit_magazine, name="magazine_edit"),
    path('magazines/<int:pk>/delete/', delete_magazine, name="magazine_delete"),
    path('magazines/genres/<int:pk>/', show_genres_details, name="genres_details"),
]
#so the empty string '' is saying what comes after 
# "books/" from the previous urls.py route. so "include"
# makes it so that's what's after 